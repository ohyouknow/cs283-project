#!/usr/bin/python

import socket

GET     = "GET"
POST    = "POST"
PUT     = "PUT"
HTTP10  = "HTTP/1.0"
HTTP11  = "HTTP/1.1"
CRLF    = "\r\n"

class HTTPDelegate(object):

    def __init__(self):
        self.onheaders = None
        self.onbody = None

#=============================================================================

class SocketReadError(Exception):
    def __init__(self, expr, msg):
        self.expr = expr
        self.msg = msg

#=============================================================================

class HTTPMessage(object):

    def __init__(self):
        self._version= HTTP10
        self._headers= {}
        self._body   = ""
        self._conn   = None
        self._raw    = ""
        self._delegate = HTTPDelegate()

    @classmethod
    def initConn(cls, conn, delegate):
        inst = cls()
        inst._conn = conn
        inst._delegate = delegate
        return inst

    def __str__(self):
        return self.toRaw()

    def toRaw(self):
        raw = ""
        for h in self._headers.keys():
            raw += h + ": " + self._headers[h] + CRLF
        raw += CRLF

        if "Transfer-Encoding" in self._headers.keys() and \
                    self._headers["Transfer-Encoding"] == "chunked":
            raw += str(hex(len(self._body)))[2:] + CRLF
            raw += self._body
            raw += "0" + CRLF
        else:
            raw += self._body

        raw += CRLF
        return raw

    def readFirst(self):
        self._raw = ''
        while self._raw[-2:] != CRLF:
            self._raw += self.safeRead(1)
    
    def readHeaders(self):
        rawheaders = ''
        while rawheaders[-4:] != CRLF + CRLF:
            rawheaders += self.safeRead(1)
         
        lines = rawheaders.split(CRLF)
        for line in lines[1:-1]:
            if ":" in line:
                self._headers[line.split(": ")[0].strip()] = line.split(": ")[1].strip()

        self._raw += rawheaders
    
    def shouldReadBody(self):
        return True

    def safeRead(self, c_len):
        tries = 0
        msg = ''
        while len(msg) != c_len:
            piece = self._conn.recv(c_len - len(msg))
            if piece == '':
                tries += 1
            else:
                msg += piece

            if tries > 3:
                raise SocketReadError(self, "Could not read from socket")

        return msg

    def readBody(self):
        if self.shouldReadBody():
            if "Content-Length" in self._headers.keys():
                c_len = int(self._headers["Content-Length"])
                self._body = self.safeRead(c_len)
            elif "Transfer-Encoding" in self._headers.keys() and \
                    self._headers["Transfer-Encoding"] == "chunked":
                while 1:
                    data = ''
                    while data[-2:] != CRLF:
                        data += self.safeRead(1)
                    data = data[:-2]
                    c_len = int(data, 16)
                    if c_len == 0:
                        break
                    else:
                        self._body += self.safeRead(c_len)
                        self.safeRead(2)
            else:
                piece = self._conn.recv(4096)
                self._body = piece
                while piece != '':
                    piece = self._conn.recv(4096)
                    self._body += piece
                         
        self._raw += self._body

    def readRaw(self):
        self.readFirst() 

        self.readHeaders()
        if self._delegate.onheaders != None:
            self._delegate.onheaders(self)

        self.readBody()
        if self._delegate.onbody != None:
            self._delegate.onbody(self)

    def run(self):
        self.readRaw()

#=============================================================================

class HTTPResponse(HTTPMessage):

    def __init__(self):
        super(HTTPResponse, self).__init__()
        self._status = 0
        self._reason = ""

    def toRaw(self):
        raw = " ".join([self._version, str(self._status), self._reason, CRLF])
        raw += super(HTTPResponse, self).toRaw()
        return raw

    def readFirst(self):
        super(HTTPResponse, self).readFirst()
        self._version = self._raw.split()[0]
        self._status = self._raw.split()[1]
        self._reason = self._raw[self._raw.find(self._status) + len(self._status) + 1:-2]

    def readRaw(self):
        super(HTTPResponse, self).readRaw()
        self._conn.close()

#=============================================================================

class HTTPRequest(HTTPMessage):

    def __init__(self):
        super(HTTPRequest, self).__init__()
        self._uri = ""
        self._method = GET
        self._reqport = 80

    def toRaw(self):
        raw = " ".join([self._method, self._uri, self._version, CRLF])
        raw += super(HTTPRequest, self).toRaw()
        return raw
   
    def readHeaders(self):
        super(HTTPRequest, self).readHeaders()
        self._headers["Proxy-Connection"] = "close"

    def shouldReadBody(self):
        return self._method == POST or self._method == PUT
    
    def readRaw(self):
        super(HTTPRequest, self).readRaw()
        self._headers["Accept-Encoding"] = ""
        self._headers["Connection"] = "close"

    def readFirst(self):
        super(HTTPRequest, self).readFirst()
        tokens = self._raw.split()
        self._method = tokens[0]
        uri = tokens[1]
        self._version = tokens[2]
      
        if "https://" in uri or ":443" in uri:
            #SSL NOT SUPPORTED
            self._reqport = 80
            uri = uri.replace("https://", "")
            uri = uri.replace(":443", "")
        
        uri = uri.replace("http://", "")
        self._headers["Host"] = uri[:uri.find("/")]
        self._uri = uri[uri.find("/"):]

    def getResponse(self, delegate):
        resp = socket.create_connection((self._headers["Host"], self._reqport))
        resp.sendall(self.toRaw())
        inst = HTTPResponse.initConn(resp, delegate)
        inst._request = self
        return inst




