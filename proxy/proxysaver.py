#!/usr/bin/python

from proxy import *
from http import *
import pymysql
from base64 import b64encode

class ProxySaver(object):
    
    def __init__(self, port, dbhost, dbuser, dbpass):
        self.port = port
        self.db = pymysql.connect(dbhost, dbuser, dbpass)
        self.proxy = Proxy(port, self.getDelegate())
   
    def getDelegate(self):
        d = HTTPDelegate()
        d.onheaders = None 
        d.onbody = self.onbody
        return d
    
    def start(self):
        self.proxy.start()

    def onbody(self, obj):
        c = self.db.cursor()
        
        if obj.__class__ == HTTPRequest:
            vals = [obj._uri, \
                      obj._method, \
                      str(obj._reqport), \
                      obj._version, \
                      str(obj._headers), \
                      b64encode(obj._body), \
                      obj._conn.getpeername()[0], \
                      str(self.port)]
            q = "insert into httpdebugger.requests \
                (URI, Method, RequestPort, HTTPVersion, Headers, Body, UserIP, UserPort) \
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            try:
                c.execute(q, vals)
                obj._dbid = c.lastrowid
            except Exception as e:
                obj._dbid = -1
                print "DB insert failed."

        elif obj.__class__ == HTTPResponse:
            vals = [str(obj._request._dbid), \
                      str(obj._status), \
                      obj._reason, \
                      obj._version, \
                      str(obj._headers), \
                      b64encode(obj._body)]
            q = "insert into httpdebugger.responses \
                (RequestID, Status, Reason, HTTPVersion, Headers, Body) \
                VALUES (%s, %s, %s, %s, %s, %s)"
            try:
                c.execute(q, vals)
            except Exception as e:
                print "DB insert failed."

        c.close()
        self.db.commit()
