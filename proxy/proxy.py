#!/usr/bin/python

import socket
from http import *
import threading
import sys

class ProxyConn(threading.Thread):

    def __init__(self, conn, delegate):
        threading.Thread.__init__(self)
        self._conn = conn
        self._delegate = delegate

    def run(self):
        try:
            req = HTTPRequest.initConn(self._conn, self._delegate)
            req.run()

            resp = req.getResponse(self._delegate)
            resp.run()

            self._conn.sendall(resp.toRaw())
        except Exception as e:
            "Ignore error.."
        finally:
            self._conn.close()

class Proxy():
    
    def __init__(self, port, delegate):
        self.delegate = delegate
        self.port = port
        self.host = '' 

    def __str__(self):
        return "Proxy Settings:\n" + \
                "IP Address: " + str(socket.gethostbyname(socket.gethostname())) + "\n" + \
                "Port: " + str(self.port) + "\n"
    
    def bind(self):
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.bind((self.host, self.port))
            self._socket.listen(1)
        except:
            print "Port is currently being used. Make sure port is free. Wait for sometime and try again."
            sys.exit(-1)

        print str(self) 

    def loop(self):
        while(1):
            conn, addr = self._socket.accept()
            pc = ProxyConn(conn, self.delegate)
            pc.start()

    def start(self):
        self.bind()
        self.loop()
