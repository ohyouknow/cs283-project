﻿namespace proxyServerData
{
    partial class InitialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.serverText = new System.Windows.Forms.TextBox();
            this.serverLabel = new System.Windows.Forms.Label();
            this.databaseText = new System.Windows.Forms.TextBox();
            this.databaseLabel = new System.Windows.Forms.Label();
            this.uidText = new System.Windows.Forms.TextBox();
            this.uidLabel = new System.Windows.Forms.Label();
            this.passwordText = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(61, 230);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(123, 55);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // serverText
            // 
            this.serverText.Location = new System.Drawing.Point(45, 32);
            this.serverText.Name = "serverText";
            this.serverText.Size = new System.Drawing.Size(155, 20);
            this.serverText.TabIndex = 3;
            this.serverText.Text = " localhost";
            this.serverText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // serverLabel
            // 
            this.serverLabel.AutoSize = true;
            this.serverLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverLabel.Location = new System.Drawing.Point(90, 9);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(55, 20);
            this.serverLabel.TabIndex = 4;
            this.serverLabel.Text = "Server";
            // 
            // databaseText
            // 
            this.databaseText.Location = new System.Drawing.Point(45, 85);
            this.databaseText.Name = "databaseText";
            this.databaseText.Size = new System.Drawing.Size(155, 20);
            this.databaseText.TabIndex = 3;
            this.databaseText.Text = "httpdebugger";
            this.databaseText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // databaseLabel
            // 
            this.databaseLabel.AutoSize = true;
            this.databaseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseLabel.Location = new System.Drawing.Point(90, 62);
            this.databaseLabel.Name = "databaseLabel";
            this.databaseLabel.Size = new System.Drawing.Size(79, 20);
            this.databaseLabel.TabIndex = 4;
            this.databaseLabel.Text = "Database";
            // 
            // uidText
            // 
            this.uidText.Location = new System.Drawing.Point(45, 138);
            this.uidText.Name = "uidText";
            this.uidText.Size = new System.Drawing.Size(155, 20);
            this.uidText.TabIndex = 3;
            this.uidText.Text = "root";
            this.uidText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // uidLabel
            // 
            this.uidLabel.AutoSize = true;
            this.uidLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uidLabel.Location = new System.Drawing.Point(107, 115);
            this.uidLabel.Name = "uidLabel";
            this.uidLabel.Size = new System.Drawing.Size(38, 20);
            this.uidLabel.TabIndex = 4;
            this.uidLabel.Text = "UID";
            // 
            // passwordText
            // 
            this.passwordText.HideSelection = false;
            this.passwordText.Location = new System.Drawing.Point(45, 188);
            this.passwordText.Name = "passwordText";
            this.passwordText.PasswordChar = '*';
            this.passwordText.Size = new System.Drawing.Size(155, 20);
            this.passwordText.TabIndex = 3;
            this.passwordText.Text = "testingString";
            this.passwordText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordLabel.Location = new System.Drawing.Point(90, 165);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(78, 20);
            this.passwordLabel.TabIndex = 4;
            this.passwordLabel.Text = "Password";
            // 
            // InitialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 305);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.uidLabel);
            this.Controls.Add(this.databaseLabel);
            this.Controls.Add(this.serverLabel);
            this.Controls.Add(this.passwordText);
            this.Controls.Add(this.uidText);
            this.Controls.Add(this.databaseText);
            this.Controls.Add(this.serverText);
            this.Controls.Add(this.connectButton);
            this.Name = "InitialForm";
            this.Text = "HttpDebugger";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox serverText;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.TextBox databaseText;
        private System.Windows.Forms.Label databaseLabel;
        private System.Windows.Forms.TextBox uidText;
        private System.Windows.Forms.Label uidLabel;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.Label passwordLabel;
    }
}

