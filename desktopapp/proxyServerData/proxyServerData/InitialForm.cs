﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proxyServerData
{
    public partial class InitialForm : Form
    {
        public InitialForm()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(this.InitialForm_close);
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            try
            {
                (new DataPacketForm(new MySql(serverText.Text,
                    databaseText.Text,
                    uidText.Text,
                    passwordText.Text))).Show();
                this.Hide();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        private void InitialForm_close(object sender, FormClosedEventArgs e) {
            Application.Exit();
        }
    }
}
