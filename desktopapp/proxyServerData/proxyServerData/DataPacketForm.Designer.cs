﻿namespace proxyServerData
{
    partial class DataPacketForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.httpTab = new System.Windows.Forms.TabControl();
            this.requestPage = new System.Windows.Forms.TabPage();
            this.requestGrid = new System.Windows.Forms.DataGridView();
            this.responsePage = new System.Windows.Forms.TabPage();
            this.responsesGrid = new System.Windows.Forms.DataGridView();
            this.reqResTab = new System.Windows.Forms.TabPage();
            this.serverLabel = new System.Windows.Forms.Label();
            this.serverText = new System.Windows.Forms.TextBox();
            this.dbLabel = new System.Windows.Forms.Label();
            this.dbText = new System.Windows.Forms.TextBox();
            this.uidLabel = new System.Windows.Forms.Label();
            this.uidText = new System.Windows.Forms.TextBox();
            this.ipLabel = new System.Windows.Forms.Label();
            this.ipText = new System.Windows.Forms.TextBox();
            this.res_reqGrid = new System.Windows.Forms.DataGridView();
            this.httpTab.SuspendLayout();
            this.requestPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.requestGrid)).BeginInit();
            this.responsePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.responsesGrid)).BeginInit();
            this.reqResTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.res_reqGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // httpTab
            // 
            this.httpTab.Controls.Add(this.requestPage);
            this.httpTab.Controls.Add(this.responsePage);
            this.httpTab.Controls.Add(this.reqResTab);
            this.httpTab.Location = new System.Drawing.Point(190, 12);
            this.httpTab.Name = "httpTab";
            this.httpTab.SelectedIndex = 0;
            this.httpTab.Size = new System.Drawing.Size(722, 526);
            this.httpTab.TabIndex = 0;
            // 
            // requestPage
            // 
            this.requestPage.Controls.Add(this.requestGrid);
            this.requestPage.Location = new System.Drawing.Point(4, 22);
            this.requestPage.Name = "requestPage";
            this.requestPage.Padding = new System.Windows.Forms.Padding(3);
            this.requestPage.Size = new System.Drawing.Size(714, 500);
            this.requestPage.TabIndex = 0;
            this.requestPage.Text = "Request";
            this.requestPage.UseVisualStyleBackColor = true;
            // 
            // requestGrid
            // 
            this.requestGrid.AllowUserToAddRows = false;
            this.requestGrid.AllowUserToDeleteRows = false;
            this.requestGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.requestGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.requestGrid.Location = new System.Drawing.Point(3, 3);
            this.requestGrid.Name = "requestGrid";
            this.requestGrid.ReadOnly = true;
            this.requestGrid.Size = new System.Drawing.Size(708, 491);
            this.requestGrid.TabIndex = 0;
            // 
            // responsePage
            // 
            this.responsePage.Controls.Add(this.responsesGrid);
            this.responsePage.Location = new System.Drawing.Point(4, 22);
            this.responsePage.Name = "responsePage";
            this.responsePage.Padding = new System.Windows.Forms.Padding(3);
            this.responsePage.Size = new System.Drawing.Size(714, 500);
            this.responsePage.TabIndex = 1;
            this.responsePage.Text = "Response";
            this.responsePage.UseVisualStyleBackColor = true;
            // 
            // responsesGrid
            // 
            this.responsesGrid.AllowUserToAddRows = false;
            this.responsesGrid.AllowUserToDeleteRows = false;
            this.responsesGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.responsesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.responsesGrid.Location = new System.Drawing.Point(3, 3);
            this.responsesGrid.Name = "responsesGrid";
            this.responsesGrid.ReadOnly = true;
            this.responsesGrid.Size = new System.Drawing.Size(708, 491);
            this.responsesGrid.TabIndex = 1;
            // 
            // reqResTab
            // 
            this.reqResTab.Controls.Add(this.res_reqGrid);
            this.reqResTab.Location = new System.Drawing.Point(4, 22);
            this.reqResTab.Name = "reqResTab";
            this.reqResTab.Padding = new System.Windows.Forms.Padding(3);
            this.reqResTab.Size = new System.Drawing.Size(714, 500);
            this.reqResTab.TabIndex = 2;
            this.reqResTab.Text = "Reqest and Response";
            this.reqResTab.UseVisualStyleBackColor = true;
            // 
            // serverLabel
            // 
            this.serverLabel.AutoSize = true;
            this.serverLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverLabel.Location = new System.Drawing.Point(63, 12);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(55, 20);
            this.serverLabel.TabIndex = 1;
            this.serverLabel.Text = "Server";
            // 
            // serverText
            // 
            this.serverText.BackColor = System.Drawing.SystemColors.Control;
            this.serverText.Location = new System.Drawing.Point(12, 35);
            this.serverText.Name = "serverText";
            this.serverText.ReadOnly = true;
            this.serverText.Size = new System.Drawing.Size(160, 20);
            this.serverText.TabIndex = 2;
            this.serverText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dbLabel
            // 
            this.dbLabel.AutoSize = true;
            this.dbLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbLabel.Location = new System.Drawing.Point(48, 63);
            this.dbLabel.Name = "dbLabel";
            this.dbLabel.Size = new System.Drawing.Size(79, 20);
            this.dbLabel.TabIndex = 1;
            this.dbLabel.Text = "Database";
            // 
            // dbText
            // 
            this.dbText.Location = new System.Drawing.Point(12, 86);
            this.dbText.Name = "dbText";
            this.dbText.ReadOnly = true;
            this.dbText.Size = new System.Drawing.Size(160, 20);
            this.dbText.TabIndex = 2;
            this.dbText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // uidLabel
            // 
            this.uidLabel.AutoSize = true;
            this.uidLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uidLabel.Location = new System.Drawing.Point(54, 117);
            this.uidLabel.Name = "uidLabel";
            this.uidLabel.Size = new System.Drawing.Size(64, 20);
            this.uidLabel.TabIndex = 1;
            this.uidLabel.Text = "User ID";
            // 
            // uidText
            // 
            this.uidText.Location = new System.Drawing.Point(12, 140);
            this.uidText.Name = "uidText";
            this.uidText.ReadOnly = true;
            this.uidText.Size = new System.Drawing.Size(160, 20);
            this.uidText.TabIndex = 2;
            this.uidText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ipLabel
            // 
            this.ipLabel.AutoSize = true;
            this.ipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel.Location = new System.Drawing.Point(50, 181);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(62, 20);
            this.ipLabel.TabIndex = 1;
            this.ipLabel.Text = "User IP";
            // 
            // ipText
            // 
            this.ipText.Location = new System.Drawing.Point(8, 204);
            this.ipText.Name = "ipText";
            this.ipText.ReadOnly = true;
            this.ipText.Size = new System.Drawing.Size(160, 20);
            this.ipText.TabIndex = 2;
            this.ipText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // res_reqGrid
            // 
            this.res_reqGrid.AllowUserToAddRows = false;
            this.res_reqGrid.AllowUserToDeleteRows = false;
            this.res_reqGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.res_reqGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.res_reqGrid.Location = new System.Drawing.Point(3, 5);
            this.res_reqGrid.Name = "res_reqGrid";
            this.res_reqGrid.ReadOnly = true;
            this.res_reqGrid.Size = new System.Drawing.Size(708, 491);
            this.res_reqGrid.TabIndex = 2;
            // 
            // DataPacketForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 550);
            this.Controls.Add(this.ipText);
            this.Controls.Add(this.ipLabel);
            this.Controls.Add(this.uidText);
            this.Controls.Add(this.uidLabel);
            this.Controls.Add(this.dbText);
            this.Controls.Add(this.dbLabel);
            this.Controls.Add(this.serverText);
            this.Controls.Add(this.serverLabel);
            this.Controls.Add(this.httpTab);
            this.Name = "DataPacketForm";
            this.Text = "DataPacketForm";
            this.httpTab.ResumeLayout(false);
            this.requestPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.requestGrid)).EndInit();
            this.responsePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.responsesGrid)).EndInit();
            this.reqResTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.res_reqGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl httpTab;
        private System.Windows.Forms.TabPage requestPage;
        private System.Windows.Forms.TabPage responsePage;
        private System.Windows.Forms.TabPage reqResTab;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.TextBox serverText;
        private System.Windows.Forms.Label dbLabel;
        private System.Windows.Forms.TextBox dbText;
        private System.Windows.Forms.Label uidLabel;
        private System.Windows.Forms.TextBox uidText;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.TextBox ipText;
        private System.Windows.Forms.DataGridView requestGrid;
        private System.Windows.Forms.DataGridView responsesGrid;
        private System.Windows.Forms.DataGridView res_reqGrid;

    }
}