﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace proxyServerData
{
    public partial class DataPacketForm : Form
    {
        MySql mySqlObj;
        private string table;
        
        public DataPacketForm(MySql sqlIn)
        {
            InitializeComponent();
            mySqlObj = sqlIn;
            serverText.Text = mySqlObj.getServer();
            dbText.Text = mySqlObj.getDatabase();
            uidText.Text = mySqlObj.getUID();
            ipText.Text = mySqlObj.getIp();
            table = "requests";

            Rebind(requestGrid);
            SetTimer();

            httpTab.SelectedIndexChanged += new EventHandler(Tabs_SelectedIndexChanged);
            this.FormClosed += new FormClosedEventHandler(this.DataPacketForm_close);
        }

        private void Tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = httpTab.SelectedIndex;
            if (i == 0)
            {
                table = "requests";
                Rebind(requestGrid);
            }
            else if (i == 1)
            {
                table = "responses";
                Rebind(responsesGrid);
            }
            else
            {
                table = "req_res";
                Rebind(res_reqGrid);
            }
        }

        private void Rebind(DataGridView grid) {
            int scrollRowVal = grid.FirstDisplayedScrollingRowIndex;
            int scrollWidthVal = grid.FirstDisplayedScrollingColumnIndex;
            BindingSource bindSource1 = new BindingSource();
            bindSource1.DataSource = (mySqlObj.getInfo(table)).Tables[0];
            grid.DataSource = bindSource1;
            if (scrollRowVal == -1)
                scrollRowVal = 0;
            if (scrollWidthVal == -1)
                scrollWidthVal = 0;
            grid.FirstDisplayedScrollingColumnIndex = scrollWidthVal;
            grid.FirstDisplayedScrollingRowIndex = scrollRowVal;
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e) {
            if (table == "requests")
                Rebind(requestGrid);
            else if (table == "responses")
                Rebind(responsesGrid);
            else
                Rebind(res_reqGrid);
        }

        private void SetTimer() {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimer.Start();
        }

        private void DataPacketForm_close(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

    }
}
