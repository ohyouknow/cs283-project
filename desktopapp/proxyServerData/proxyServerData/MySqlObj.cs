﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Net;
using System.IO;
using System.Data;
using System.Windows.Forms;

namespace proxyServerData
{
    public class MySql
    {		
		private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        private string ip;
        private MySqlDataAdapter adapter;
        
        public MySql(string serverIn, string dbIn, string uidIn, string passwordIn) {
            server = serverIn;
            database = dbIn;
            uid = uidIn;
            password = passwordIn;
            findIp();
            Connect();
        }


        public DataSet getInfo(string table) {
            string query = "";
            if(table == "req_res")
                query =  "SELECT * FROM httpdebugger.responses LEFT JOIN httpdebugger.requests ON (httpdebugger.responses.RequestID = httpdebugger.requests.RequestID)";
            else
                query = "SELECT * FROM " + table;
            adapter = new MySqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            return ds;
        }


    #region getMethods
        public string getServer() {
            return server;
        }

        public string getDatabase() {
            return database;
        }

        public string getUID() {
            return uid;
        }

        public string getIp() {
            return ip;
        }
    #endregion

    #region privateMethods
        private void Connect() {
            try
            {
                string connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

                connection = new MySqlConnection(connectionString);
                connection.Open();
            }
            catch (MySqlException ex){
                throw new Exception(ex.ToString());
            }
        }

        private void findIp()
        {
            try
            {
                ip = "";
                WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
                using (WebResponse response = request.GetResponse())
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    ip = stream.ReadToEnd();
                }

                //Search for the ip in the html
                int first = ip.IndexOf("Address: ") + 9;
                int last = ip.LastIndexOf("</body>");
                ip = ip.Substring(first, last - first);
            }
            catch (Exception ex) {
                throw new Exception(ex.ToString());
            }

        }
    #endregion

    }
}
