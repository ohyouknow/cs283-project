﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.httpdebuggerDataSet = new Test.httpdebuggerDataSet();
            this.requestsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.requestsTableAdapter = new Test.httpdebuggerDataSetTableAdapters.requestsTableAdapter();
            this.requestIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uRIDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.methodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestPortDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hTTPVersionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.headersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bodyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userIPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userPortDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.httpdebuggerDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.requestIDDataGridViewTextBoxColumn,
            this.uRIDataGridViewTextBoxColumn,
            this.methodDataGridViewTextBoxColumn,
            this.requestPortDataGridViewTextBoxColumn,
            this.hTTPVersionDataGridViewTextBoxColumn,
            this.headersDataGridViewTextBoxColumn,
            this.bodyDataGridViewTextBoxColumn,
            this.userIPDataGridViewTextBoxColumn,
            this.userPortDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.requestsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(260, 238);
            this.dataGridView1.TabIndex = 0;
            // 
            // httpdebuggerDataSet
            // 
            this.httpdebuggerDataSet.DataSetName = "httpdebuggerDataSet";
            this.httpdebuggerDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // requestsBindingSource
            // 
            this.requestsBindingSource.DataMember = "requests";
            this.requestsBindingSource.DataSource = this.httpdebuggerDataSet;
            // 
            // requestsTableAdapter
            // 
            this.requestsTableAdapter.ClearBeforeFill = true;
            // 
            // requestIDDataGridViewTextBoxColumn
            // 
            this.requestIDDataGridViewTextBoxColumn.DataPropertyName = "RequestID";
            this.requestIDDataGridViewTextBoxColumn.HeaderText = "RequestID";
            this.requestIDDataGridViewTextBoxColumn.Name = "requestIDDataGridViewTextBoxColumn";
            // 
            // uRIDataGridViewTextBoxColumn
            // 
            this.uRIDataGridViewTextBoxColumn.DataPropertyName = "URI";
            this.uRIDataGridViewTextBoxColumn.HeaderText = "URI";
            this.uRIDataGridViewTextBoxColumn.Name = "uRIDataGridViewTextBoxColumn";
            // 
            // methodDataGridViewTextBoxColumn
            // 
            this.methodDataGridViewTextBoxColumn.DataPropertyName = "Method";
            this.methodDataGridViewTextBoxColumn.HeaderText = "Method";
            this.methodDataGridViewTextBoxColumn.Name = "methodDataGridViewTextBoxColumn";
            // 
            // requestPortDataGridViewTextBoxColumn
            // 
            this.requestPortDataGridViewTextBoxColumn.DataPropertyName = "RequestPort";
            this.requestPortDataGridViewTextBoxColumn.HeaderText = "RequestPort";
            this.requestPortDataGridViewTextBoxColumn.Name = "requestPortDataGridViewTextBoxColumn";
            // 
            // hTTPVersionDataGridViewTextBoxColumn
            // 
            this.hTTPVersionDataGridViewTextBoxColumn.DataPropertyName = "HTTPVersion";
            this.hTTPVersionDataGridViewTextBoxColumn.HeaderText = "HTTPVersion";
            this.hTTPVersionDataGridViewTextBoxColumn.Name = "hTTPVersionDataGridViewTextBoxColumn";
            // 
            // headersDataGridViewTextBoxColumn
            // 
            this.headersDataGridViewTextBoxColumn.DataPropertyName = "Headers";
            this.headersDataGridViewTextBoxColumn.HeaderText = "Headers";
            this.headersDataGridViewTextBoxColumn.Name = "headersDataGridViewTextBoxColumn";
            // 
            // bodyDataGridViewTextBoxColumn
            // 
            this.bodyDataGridViewTextBoxColumn.DataPropertyName = "Body";
            this.bodyDataGridViewTextBoxColumn.HeaderText = "Body";
            this.bodyDataGridViewTextBoxColumn.Name = "bodyDataGridViewTextBoxColumn";
            // 
            // userIPDataGridViewTextBoxColumn
            // 
            this.userIPDataGridViewTextBoxColumn.DataPropertyName = "UserIP";
            this.userIPDataGridViewTextBoxColumn.HeaderText = "UserIP";
            this.userIPDataGridViewTextBoxColumn.Name = "userIPDataGridViewTextBoxColumn";
            // 
            // userPortDataGridViewTextBoxColumn
            // 
            this.userPortDataGridViewTextBoxColumn.DataPropertyName = "UserPort";
            this.userPortDataGridViewTextBoxColumn.HeaderText = "UserPort";
            this.userPortDataGridViewTextBoxColumn.Name = "userPortDataGridViewTextBoxColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.httpdebuggerDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private httpdebuggerDataSet httpdebuggerDataSet;
        private System.Windows.Forms.BindingSource requestsBindingSource;
        private httpdebuggerDataSetTableAdapters.requestsTableAdapter requestsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn requestIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uRIDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn methodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn requestPortDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hTTPVersionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn headersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bodyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userIPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userPortDataGridViewTextBoxColumn;
    }
}

