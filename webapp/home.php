<!--Matthew Bucci mrb327 for CS 283 Project-->
<html>
 <head>
  <title>HTTP DEBUGGER</title>
 </head>
 <body>
 
 <?php
// Create connection
$con=mysqli_connect("localhost","root","password","httpdebugger"); //RESET THIS PASSWORD

// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
?> 
<script type = "text/javascript">
 function loadPacket(num) {
	baseURL = document.URL.split("?");
    window.location = baseURL[0] + "?packetID=" + num;
 }
 </script>
 <div id="packetListing">
<header>
<h1>IP Packets</h1>
</header>
<table class='hovertable'>
<tr>
<th width="05%">Method</th>
<th width="11%">Host</th>
</tr>

<?php
isset($_GET['packetID'])? $packetID=$_GET['packetID']:$packetID=0;
$selection = $packetID;
$query=mysqli_query($con,"SELECT * From requests");

$alternate=0;

while ($row=mysqli_fetch_array($query))
{
	if($selection > 0 && $row['RequestID'] == $selection){
		$alternate++;
		if ($alternate==3)
		$alternate=1;
		echo "<tr class=class$alternate 
		onmouseover=\"this.style.backgroundColor='#ffff66';\"
		onmouseout=\"this.style.backgroundColor='#c0c0c0';\"
		onclick=\"loadPacket(".$row['RequestID'].");\
		this.backgroundColor = c0c0c0;\">";
		echo "<td width=\"11%\">" . $row['Method'] . "</td>";
		$HeadersHost = explode("Host", $row['Headers']);
		$Host = explode("'", $HeadersHost[1]);
		echo "<td width=\"12%\">" . $Host[2] . "</td>";
	}
	else{
		$alternate++;
		if ($alternate==3)
		$alternate=1;
		echo "<tr class=class$alternate 
		onmouseover=\"this.style.backgroundColor='#ffff66';\"
		onmouseout=\"this.style.backgroundColor='#ffffff';\"
		onclick=\"loadPacket(".$row['RequestID'].");\">";
		echo "<td width=\"11%\">" . $row['Method'] . "</td>";
		$HeadersHost = explode("Host", $row['Headers']);
		$Host = explode("'", $HeadersHost[1]);
		echo "<td width=\"12%\">" . $Host[2] . "</td>";
	}
}
?>
</table>
</div>

<div id="infoTable">
<header>
<h3>Details</h3>
</header>
 <?php
isset($_GET['packetID'])? $packetID=$_GET['packetID']:$packetID=0;
$selection = $packetID;
if($selection > 0){
	$query=mysqli_query($con,"SELECT * From requests where requestID = $selection");


	while ($row=mysqli_fetch_array($query))
	{
		echo "URI: ". $row['URI'] . "<br />\n";
		echo "Method: " . $row['Method'] . "<br />\n";
		
		echo "Request Port: " . $row['RequestPort'] . "<br />\n";
		echo "HTTP Version: " . $row['HTTPVersion'] . "<br />\n";
		
		$HeadersAcceptLanguage = explode("Accept-Language", $row['Headers']);
		$AcceptLanguage = explode("'", $HeadersAcceptLanguage[1]);
		echo "Accept-Language: " . $AcceptLanguage[2] . "<br />\n";
		
		$HeadersAcceptEncoding = explode("Accept-Encoding", $row['Headers']);
		$AcceptEncoding = explode("'", $HeadersAcceptEncoding[1]);
		echo "Accept-Encoding: " . $AcceptEncoding[2] . "<br />\n";
		
		$HeadersConnection = explode("Connection", $row['Headers']);
		$Connection = explode("'", $HeadersConnection[1]);
		echo "Connection: " . $Connection[2] . "<br />\n";
		
		$HeadersAccept = explode("'Accept'", $row['Headers']);
		$Accept = explode("'", $HeadersAccept[1]);
		echo "Accept: " . $Accept[1] . "<br />\n";
		
		$HeadersUserAgent = explode("User-Agent", $row['Headers']);
		$UserAgent = explode("'", $HeadersUserAgent[1]);
		echo "User-Agent: " . $UserAgent[2] . "<br />\n";
		
		$HeadersHost = explode("Host", $row['Headers']);
		$Host = explode("'", $HeadersHost[1]);
		echo "Host: " . $Host[2] . "<br />\n";
		
		$HeadersReferer = explode("Referer", $row['Headers']);
		$Referer = explode("'", $HeadersReferer[1]);
		echo "Referer: " . $Referer[2] . "<br />\n";
		
		$HeadersCookie = explode("Cookie", $row['Headers']);
		$Cookie = explode("'", $HeadersCookie[1]);
		echo "Cookie: " . $Cookie[2] . "<br />\n";
		
		$HeadersProxyConnection = explode("Proxy-Connection", $row['Headers']);
		$ProxyConnection = explode("'", $HeadersProxyConnection[1]);
		echo "Proxy-Connection: " . $ProxyConnection[2] . "<br />\n";
		
		echo "User IP: " . $row['UserIP'] . "<br />\n";
		echo "User Port: " . $row['UserPort'] . "<br />\n";
	}
}
else{
	echo "No packet selected";
}
?>
</div>

<div id="rawDataTable">
<header>
<h3>Body</h3>
</header>
 <?php
isset($_GET['packetID'])? $packetID=$_GET['packetID']:$packetID=0;
$selection = $packetID;
if($selection > 0){
	$query=mysqli_query($con,"SELECT * From requests where requestID = $selection");

	while ($row=mysqli_fetch_array($query))
	{
		echo wordwrap(base64_decode($row['Body']), 30, "<br />\n", true);
	}
}
else{
	echo "No packet selected";
}
?>
</div>
</body>
</html>

<style>
#packetListing
{
    width:100%;
    height:18cm;
    overflow:scroll;
}
#infoTable
{
	float:left;
    width:50%;
    height:18cm;
    overflow:scroll;
}
#rawDataTable
{
	float:right;
    width:50%;
    height:18cm;
    overflow:scroll;
}
</style>