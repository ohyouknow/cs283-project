// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var starter = angular.module('starter', [
'ionic',
'ui.router'
])
.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'partials/main.html',
            controller: function($scope) {$scope.title="Traffic Sniffer";}
        })
        .state('settings', {
          url: '/settings',
          templateUrl: 'partials/settings.html',
          controller: function($scope) {$scope.title="Settings";}
        })
        .state('filter', {
          url: '/filter',
          templateUrl: 'partials/filter.html',
          controller: function($scope) {$scope.title="Filter";}
        })
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit
        });

}).factory('filteringService', function($rootScope) {
        var sharedFilteringService = {};

        sharedFilteringService.filter = {ip: null, port: null, phrase: null};

          // update the filter ip
        sharedFilteringService.updateIP = function(ip) {
          var fIP = null;
          if (ip !== undefined) fIP = ip;
          else if (ip.trim() == '') ip = null;

          this.filter.ip = fIP;

          this.broadcastItem();
        };

        // update the filter port
        sharedFilteringService.updatePort = function(port) {
          var fPort = null;
          if (port !== undefined) fPort = port;
          else if (port.trim() == '') fPort = null;

          this.filter.port = fPort;

          this.broadcastItem();
        };

          // updates the search phrase
        sharedFilteringService.updateContentPhrase = function(phrase) {
          var fPhrase = null;
          if (phrase !== undefined) fPhrase = phrase;
          else if (phrase.trim() == '') phrase = null;

          this.filter.phrase = fPhrase;

          this.broadcastItem();
        };

          // broadcast the subscribers (none for now)
        sharedFilteringService.broadcastItem = function() {
          $rootScope.$broadcast('handleFilteringUpdated');
        };

        return sharedFilteringService;
}).factory('headerService', function($rootScope) {
      var sharedHeaderService = {};

      sharedHeaderService.buttonLeft = {class: null, callback: null};

        // updates the left button
      sharedHeaderService.updateLeftButton = function(btnClass, callback) {
        sharedHeaderService.buttonLeft = {class: btnClass, callback: callback};
        sharedHeaderService.broadcastItem();
      };

          // broadcast the subscribers (the header for now)
        sharedHeaderService.broadcastItem = function() {
          $rootScope.$broadcast('handleHeaderUpdated');
        };

      return sharedHeaderService;
    })
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

//starter;