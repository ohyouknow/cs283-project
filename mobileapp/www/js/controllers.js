//var mainControllers = angular.module('mainControllers', []);

starter.controller('MainCtrl', ['$scope', '$http',
  function ($scope, $http) {

    $scope.orderProp = 'age';
    $scope.title="Data Traffic Sniffer"
  }])
.controller("HeaderCtrl", ['$scope', '$http', '$ionicPlatform', 'headerService', '$ionicSideMenuDelegate',
      function ($scope, $http, $ionicPlatform, headerService, $ionicSideMenuDelegate) {



      $scope.goBack = function () {
          history.back();
      }

      // have the "go Back" button by default
        if (headerService.buttonLeft.class == null && headerService.buttonLeft.callback == null) {
            $scope.leftButtonClass = 'button button-icon icon ion-ios7-arrow-back';
            $scope.leftButtonCallback = $scope.goBack;
            headerService.updateLeftButton( $scope.leftButtonClass, $scope.leftButtonCallback);
        } else {
            $scope.leftButtonClass = headerService.buttonLeft.class;
            $scope.leftButtonCallback = headerService.buttonLeft.callback;
        }

        // update the left header button once it was changed by another view
        $scope.$on('handleHeaderUpdated', function(event) {
            $scope.leftButtonClass = headerService.buttonLeft.class;
            $scope.leftButtonCallback = headerService.buttonLeft.callback;
        });

        // initialize the menu
          $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
          };


       $ionicPlatform.registerBackButtonAction($scope.goBack, 100);

}])
.controller("SettingsCtrl", ['$scope', '$http',
      function ($scope, $http) {

      $scope.save = function () {
          history.back();
      }
}])
.controller("HomeCtrl", ['$scope', '$http', 'filteringService', 'headerService',
      function ($scope, $http, sharedFilteringService, headerService) {

        // creating the model for the search field. Populating with the session value
        $scope.search = {phrase: sharedFilteringService.filter.phrase};

        // ordering by default
        $scope.searchPredicate = 'time';
        $scope.searchReverse = false; // descending

        // initializing the left header icon with a menu
        headerService.updateLeftButton('button button-icon icon ion-navicon', function() {});

      $scope.rawPackages = [
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:04"},
        {ip: "10.0.0.1", port:80, content:"akfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf", time:"10:02"},
        {ip: "10.0.0.1", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf defferent", time:"10:02"},
        {ip: "10.0.0.2", port:80, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf defferent", time:"10:02"},
        {ip: "10.0.0.2", port:81, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf defferent", time:"10:02"},
        {ip: "10.0.0.1", port:81, content:"kfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklfkfjsalfjaskfjasldfjasklf defferent", time:"10:02"},
      ];
      $scope.filteredPackages = [];
        /**
        ** This function is used for content search
        **/
      $scope.searchContent = function () {
        var results = [];
        var inputPhrase = $scope.search.phrase;

        if (inputPhrase == sharedFilteringService.filter.contentPhrase) {
            $scope.packages = angular.copy($scope.filteredPackages);
            return true;
        }

        // update the search criteria
        sharedFilteringService.updateContentPhrase(inputPhrase);
        console.log(sharedFilteringService);

        var i=0;

        // look through the raw packages
        for (; i<$scope.filteredPackages.length; i++) {
          if ($scope.filteredPackages[i].content.toLowerCase().indexOf(inputPhrase.toLowerCase()) >= 0)
            results.push($scope.filteredPackages[i]);
        }

        // updates the model
        $scope.packages = angular.copy(results);
      }

        /**
        **  This function is used to filter the raw packages based on the settings made in the filters screen
        **/
      $scope.filter = function(ip, port) {
        var results = [];
        var i=0;

        // look through the raw packages
        for (; i<$scope.rawPackages.length; i++) {
            var include = true;
            if (ip != null && $scope.rawPackages[i].ip != ip)
                include = false;

            if (port != null && $scope.rawPackages[i].port != port)
                include = false;

            if (include)
                results.push($scope.rawPackages[i]);
        }

        // updates the model
        $scope.filteredPackages = angular.copy(results);

        // filter for content for final results
        $scope.searchContent();
      }
      $scope.filter(sharedFilteringService.filter.ip, sharedFilteringService.filter.port);

}])
.controller("FilterCtrl", ['$scope', '$http', 'filteringService',
  function ($scope, $http, sharedFilteringService) {

    $scope.filter = {ip: sharedFilteringService.filter.ip, port: sharedFilteringService.filter.port}


  $scope.save = function () {
      if ($scope.filter === undefined) {
        sharedFilteringService.updateIP(null);
        sharedFilteringService.updatePort(null);
      } else {
        sharedFilteringService.updateIP($scope.filter.ip);
        sharedFilteringService.updatePort($scope.filter.port);
      }
      history.back();
  }
}]);